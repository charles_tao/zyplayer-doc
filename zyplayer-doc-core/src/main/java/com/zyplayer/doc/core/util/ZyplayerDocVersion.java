package com.zyplayer.doc.core.util;

/**
 * zyplayer-doc版本号
 * @since 2021-06-06
 */
public class ZyplayerDocVersion {
	public static final String version = "1.0.9";
}
